import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import {environment} from '../../environments/environment';
import {Event} from '../classes/event';

@Injectable({ providedIn: 'root' })
export class EventService {
  private apiUrl = environment.api_url;

  constructor(private http: HttpClient) { }

  getNextEvent(): Promise<Event> {
    return firstValueFrom(this.http.get(this.apiUrl + '/event/nextEvent'))
      .then((response: any) => {
        if (response.nextEvent !== null) {
          return new Event(response.nextEvent);
        }
      })
      .catch(this.handleError);
  }

  getUpcomingEvents(): Promise<Event[]> {
    return firstValueFrom(this.http.get(this.apiUrl + '/event/upcoming'))
      .then((response: any) => {
        if (response.events !== null) {
          const eventArray = [];
          for (const event of response.events) {
            eventArray.push(new Event(event));
          }
          return eventArray;
        } else {
          Promise.reject('no event');
        }
      })
      .catch(this.handleError);
  }

  getAllEvents(): Promise<Event[]> {
    const headers = new HttpHeaders({
      'Authorization': `${localStorage.getItem('token-type')} ${localStorage.getItem('access-token')}`
    });

    return firstValueFrom(this.http.get(this.apiUrl + '/event/allEvents', { headers }))
      .then((response: any) => {
        if (response.events !== null) {
          const eventArray = [];
          for (const event of response.events) {
            eventArray.push(new Event(event));
          }
          return eventArray;
        } else {
          Promise.reject('no event');
        }
      })
      .catch(this.handleError);
  }

  updateDates(eventIdWithDates: any) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `${localStorage.getItem('token-type')} ${localStorage.getItem('access-token')}`
    });

    return firstValueFrom(this.http.post(this.apiUrl + '/event/update/dates', eventIdWithDates, { headers }))
      .then(() => {
        return Promise.resolve();
      })
      .catch(this.handleError);
  }



  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

}
