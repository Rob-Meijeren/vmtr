import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class ContactService {
  private apiUrl: string = environment.api_url;

  constructor(private http: HttpClient) { }

  sendForm(name, email, phone, subject, message): Promise<string> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });

    const paramObj = {
      name,
      email,
      phone,
      subject,
      message
    };

    return firstValueFrom(this.http.post(this.apiUrl + '/contact/contact', JSON.stringify(paramObj), { headers }))
      .then(() => {
        return 'Uw email is verstuurd. Er word zo snel mogelijk contact opgenomen';
      })
      .catch(this.handleError);
  }

  becomeMember(name, email, phone): Promise<string> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });

    const paramObj = {
      name,
      email,
      phone
    };

    return firstValueFrom(this.http.post(this.apiUrl + '/contact/signup', JSON.stringify(paramObj), { headers }))
      .then(() => {
        return 'Uw email is verstuurd. Er word zo snel mogelijk contact opgenomen';
      })
      .catch(this.handleError);
  }

  requestTent(name, email, phone, message): Promise<string> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });

    const paramObj = {
      name,
      email,
      phone,
      message
    };

    return firstValueFrom(this.http.post(this.apiUrl + '/contact/tent', JSON.stringify(paramObj), { headers }))
      .then(() => {
        return 'Uw email is verstuurd. Er word zo snel mogelijk contact opgenomen';
      })
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

}
