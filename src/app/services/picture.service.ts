import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {UploadFile} from '../classes/upload-file';
import {Photo} from '../classes/photo';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { firstValueFrom } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class PictureService {
  private apiUrl = environment.api_url;

  constructor(private http: HttpClient) {}

  uploadPicture(albumId: string|number, image: UploadFile): Promise<string> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `${localStorage.getItem('token-type')} ${localStorage.getItem('access-token')}`
    });

    const params = {
      'albumId': albumId,
      'picture': image
    };

    return firstValueFrom(this.http.post(this.apiUrl + '/photo/upload',  JSON.stringify(params), { headers }))
      .then((response: any) => {
        if (response.photo) {
          return Promise.resolve(response.photo + ' is geupload');
        } else if (response.error) {
          return Promise.reject(response.photo + ' is niet geupload. Probeer het nogmaals of contacteer de webmaster.');
        }
      })
      .catch(this.handleError);
  }

  deletePicture(photo: Photo): Promise<void> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `${localStorage.getItem('token-type')} ${localStorage.getItem('access-token')}`
    });

    const params = {
      'photoId': photo.id
    };

    return firstValueFrom(this.http.post(this.apiUrl + '/photo/remove',  JSON.stringify(params), { headers }))
      .then(() => {
        Promise.resolve();
      })
      .catch(this.handleError);
  }

  getPhotosOfAlbum(albumId: number): Promise<Array<Photo>> {
    const headers = new HttpHeaders({
      'Authorization': `${localStorage.getItem('token-type')} ${localStorage.getItem('access-token')}`
    });

    return firstValueFrom(this.http.get(this.apiUrl + '/photo/specific/' + albumId, { headers }))
      .then((response: any) => {
        if (response.photos) {
          const photoArray = [];
          for (const photo of response.photos) {
            photoArray.push(new Photo(photo));
          }
          return Promise.resolve(photoArray);
        } else if (response.error) {
          return Promise.reject(response.error);
        }
      })
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

}
