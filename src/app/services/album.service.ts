import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import {environment} from '../../environments/environment';
import {Album} from '../classes/album';

@Injectable({ providedIn: 'root' })
export class AlbumService {
  private apiUrl = environment.api_url;

  constructor(private http: HttpClient) {}

  getAllAlbums(): Promise<Array<Album>> {
    const headers = new HttpHeaders({
      'Authorization': `${localStorage.getItem('token-type')} ${localStorage.getItem('access-token')}`
    });

    return firstValueFrom(this.http.get(this.apiUrl + '/album/all', { headers }))
      .then((response : any) => {
        if (response.albums) {
          const albumArray = [];
          for (const album of response.albums) {
            albumArray.push(new Album(album));
          }
          return Promise.resolve(albumArray);
        } else if (response.error) {
          return Promise.reject(response.error);
        }
      })
      .catch(this.handleError);
  }

  createAlbum(albumName, albumDescription): Promise<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `${localStorage.getItem('token-type')} ${localStorage.getItem('access-token')}`
    });

    const paramObj = {
      'albumName': albumName,
      'albumDescription': albumDescription
    };

    return firstValueFrom(this.http.post(this.apiUrl + '/album/create',  JSON.stringify(paramObj), { headers: headers }))
      .then((response : any) => {
        if (response.result) {
          return Promise.resolve(response);
        } else if (response.error) {
          return Promise.reject(response.error);
        }
      })
      .catch(this.handleError);
  }

  getPublicAlbums(): Promise<Array<Album>> {
    return firstValueFrom(this.http.get(this.apiUrl + '/album/public'))
      .then((response : any) => {
        if (response.albums) {
          const albumArray = [];
          for (const album of response.albums) {
            albumArray.push(new Album(album));
          }
          return Promise.resolve(albumArray);
        } else if (response.error) {
          return Promise.reject(response.error);
        }
      })
      .catch(this.handleError);
  }

  getSpecificAlbum(albumId: string): Promise<Album> {
    return firstValueFrom(this.http.get(this.apiUrl + '/album/specific/' + albumId))
      .then((response : any) => {
        if (response.album) {
          return Promise.resolve(new Album(response.album));
        } else if (response.error) {
          return Promise.reject(response.error);
        }
      })
      .catch(this.handleError);
  }

  markAsPublic(album: Album): Promise<boolean> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `${localStorage.getItem('token-type')} ${localStorage.getItem('access-token')}`
    });

    const paramObj = {
      'albumId': album.id,
    };

    return firstValueFrom(this.http.post(this.apiUrl + '/album/mark/public',  JSON.stringify(paramObj), { headers: headers }))
      .then(() => {
          album.public = 1;
          return Promise.resolve(true);
      })
      .catch(this.handleError);
  }

  markAsPrivate(album: Album): Promise<boolean> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `${localStorage.getItem('token-type')} ${localStorage.getItem('access-token')}`
    });

    const paramObj = {
      'albumId': album.id,
    };

    return firstValueFrom(this.http.post(this.apiUrl + '/album/mark/private',  JSON.stringify(paramObj), { headers: headers }))
      .then(() => {
        album.public = 0;
        return Promise.resolve(true);
      })
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }
}
