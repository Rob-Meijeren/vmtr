import { Injectable } from '@angular/core';
import {Comment} from '../classes/comment';
import {environment} from '../../environments/environment';
import { firstValueFrom } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class GuestBookService {
  private apiUrl = environment.api_url;

  constructor(private http: HttpClient) { }

  getAllComments(): Promise<Array<Comment>> {
    return firstValueFrom(this.http.get(this.apiUrl + '/guestbook/getAllComments'))
      .then((response: any) => {
        const commentArray = [];
        for (const comment of response.comments) {
          commentArray.push(new Comment(comment));
        }
        return commentArray;
      })
      .catch(this.handleError);
  }

  placeComment(name, message): Promise<Comment> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });

    const paramObj = {
      'name': name,
      'message': message
    };

    return firstValueFrom(this.http.post(this.apiUrl + '/guestbook/comment', JSON.stringify(paramObj), { headers: headers}))
      .then((response: any) => {
        if (response.newComment !== null) {
          return new Comment(response.newComment);
        } else {
          Promise.reject('no comment');
        }
      })
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }
}
