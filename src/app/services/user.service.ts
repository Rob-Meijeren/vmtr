import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import {environment} from '../../environments/environment';
import {User} from '../classes/user';

@Injectable({ providedIn: 'root' })
export class UserService {
  private apiUrl = environment.api_url;
  private user: User;

  constructor(private http: HttpClient) {}

  getUser(): Promise<User> {
    if (this.user) {
      return Promise.resolve(this.user);
    } else {
      return this.retrieveUser().then(() => {
        return Promise.resolve(this.user);
      }, () => {
        return Promise.reject(null);
      });
    }
  }

  private retrieveUser(): Promise<User> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `${localStorage.getItem('token-type')} ${localStorage.getItem('access-token')}`
    });

    return firstValueFrom(this.http.get(this.apiUrl + '/user/user', { headers }))
      .then((response : any) => {
        if (response.id) {
          this.user = new User(response);
          return this.user;
        } else {
          Promise.reject('no user');
        }
      })
      .catch(this.handleError);
  }

  updateUser(user: User): Promise<string> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `${localStorage.getItem('token-type')} ${localStorage.getItem('access-token')}`
    });

    const paramObj = {
      username: user.name,
      email: user.email
    };

    return firstValueFrom(this.http.post(this.apiUrl + '/user/update',  JSON.stringify(paramObj), { headers }))
      .then((response : any) => {
        if (response.result) {
          return response.result;
        }
      })
      .catch(this.handleError);
  }

  updatePassword(oldPassword: string, password: string): Promise<string> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `${localStorage.getItem('token-type')} ${localStorage.getItem('access-token')}`
    });

    const paramObj = {
      oldPassword,
      password
    };

    return firstValueFrom(this.http.post(this.apiUrl + '/user/updatePassword',  JSON.stringify(paramObj), { headers }))
      .then((response : any) => {
        if (response.result) {
          return Promise.resolve(response.result);
        } else if (response.error) {
          return Promise.reject(response.error);
        }
      })
      .catch(this.handleError);
  }

  logout(): Promise<boolean> {
    const headers = new HttpHeaders({
      'Authorization': `${localStorage.getItem('token-type')} ${localStorage.getItem('access-token')}`
    });

    return firstValueFrom(this.http.get(this.apiUrl + '/user/logout', { headers }))
      .then((response : any) => {
        if (response) {
          localStorage.removeItem('access-token');
          localStorage.removeItem('refresh-token');
          localStorage.removeItem('token-type');
          this.user = null;

          return Promise.resolve(true);
        } else if (response.error) {
          return Promise.reject(false);
        }
      })
      .catch(this.handleError);
  }

  login(username, password): Promise<boolean> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });

    const paramObj = {
      username,
      password
    };

    return firstValueFrom(this.http.post(this.apiUrl + '/user/login',  paramObj, { headers }))
      .then((response : any) => {
        localStorage.setItem('access-token', response.access_token);
        localStorage.setItem('remember-token', response.refresh_token);
        localStorage.setItem('token-type', response.token_type);
        setTimeout(() => {
          this.getUser();
          return true;
        } , 1000);
      })
      .catch(this.handleError);
  }

  register(name, email, password): Promise<boolean> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });

    const paramObj = {
      'name': name,
      'email': email,
      'password': password
    };

    return firstValueFrom(this.http.post(this.apiUrl + '/user/create',  JSON.stringify(paramObj), { headers }))
      .then((response : any) => {
        if(response.success) {
          return response.success;
        } else {
          return false;
        }
      })
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }
}
