import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {Route} from '../classes/route';
import {UploadFile} from '../classes/upload-file';
import {DownloadRoute} from '../classes/download-route';
import { saveAs } from 'file-saver';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { firstValueFrom } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class RouteService {
  private routeUrl = environment.api_url + '/route';

  constructor(private http: HttpClient) {}

  getPublicRoutes(): Promise<Array<Route>> {
    return firstValueFrom(this.http.get(this.routeUrl + '/public'))
      .then((response: any) => {
        if (response.routes !== null) {
          const routeArray = [];
          for (const route of response.routes) {
            routeArray.push(new Route(route));
          }
          return routeArray;
        } else {
          Promise.reject('no event');
        }
      })
      .catch(this.handleError);
  }

  getAllRoutes(): Promise<Array<Route>> {
    const headers = new HttpHeaders({
      'Authorization': `${localStorage.getItem('token-type')} ${localStorage.getItem('access-token')}`
    });

    return firstValueFrom(this.http.get(this.routeUrl + '/all', { headers }))
      .then((response: any) => {
        if (response.routes !== null) {
          const routeArray = [];
          for (const route of response.routes) {
            routeArray.push(new Route(route));
          }
          return routeArray;
        } else {
          Promise.reject('Er zijn nog geen routes geupload');
        }
      })
      .catch(this.handleError);
  }

  getSpecificRoute(routeName): Promise<Route> {
      return firstValueFrom(this.http.get(this.routeUrl + '/specific/' + routeName))
        .then((response: any) => {
          if (response.route !== null) {
            return new Route(response.route[0]);
          } else {
            Promise.reject('Dit is geen geldige route');
          }
        }).catch(this.handleError);
  }

  uploadRoute(route: UploadFile): Promise<string> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `${localStorage.getItem('token-type')} ${localStorage.getItem('access-token')}`
    });

    const params = {
      'route': route
    };

    return firstValueFrom(this.http.post(this.routeUrl + '/upload',  JSON.stringify(params), { headers }))
      .then((response: any) => {
        if (response.route === 'duplicate') {
          return Promise.resolve(route.filename + ' is al eens geupload.');
        } else if (response.error) {
          return Promise.reject(route.filename + ' heeft niet kunnenn uploaden. Probeer nogmaals of contacteer de webmaster');
        } else {
          return Promise.resolve(route.filename + ' is geupload');
        }
      })
      .catch(this.handleError);
  }

  markAsPrivate(route: Route): Promise<boolean> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `${localStorage.getItem('token-type')} ${localStorage.getItem('access-token')}`
    });

    const params = {
      'filename': route.filename
    };

    return firstValueFrom(this.http.post(this.routeUrl + '/mark/private',  JSON.stringify(params), { headers }))
      .then(() => {
        route.public = 0;
        return true;
      })
      .catch(this.handleError);
  }

  markAsPublic(route: Route): Promise<boolean> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `${localStorage.getItem('token-type')} ${localStorage.getItem('access-token')}`
    });

    const params = {
      'filename': route.filename
    };

    return firstValueFrom(this.http.post(this.routeUrl + '/mark/public',  JSON.stringify(params), { headers }))
      .then(() => {
        route.public = 1;
        return true;
      })
      .catch(this.handleError);
  }

  downloadGPX(filename: string): Promise<any> {
    return firstValueFrom(this.http.get(this.routeUrl + '/downloadGPX/' + filename))
      .then(file => {
        const downloadFile = new DownloadRoute(file);
        saveAs(new Blob([downloadFile.body], { type: 'application/xml'}), filename + '.gpx');
      }).catch(this.handleError);
  }

  downloadKML(filename: string): Promise<any> {
    return firstValueFrom(this.http.get(this.routeUrl + '/downloadKML/' + filename))
      .then(file => {
        const downloadFile = new DownloadRoute(file);
        saveAs(new Blob([downloadFile.body], { type: 'application/xml'}), filename + '.kml');
      }).catch(this.handleError);
  }

  downloadITN(filename: string): Promise<any> {
    return firstValueFrom(this.http.get(this.routeUrl + '/downloadITN/' + filename))
      .then(file => {
        const downloadFile = new DownloadRoute(file);
        saveAs(new Blob([downloadFile.body], { type: 'application/xml'}), filename + '.itn');
      }).catch(this.handleError);
  }

  checkFilePresence(filename: string): Promise<any> {
    return firstValueFrom(this.http.get(this.routeUrl + '/checkFilePresence/' + filename))
      .then((response: any) => {
        return response.present;
      }).catch(this.handleError);
  }

  subscribeMaillist(name: string, email: string): Promise<boolean|string> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `${localStorage.getItem('token-type')} ${localStorage.getItem('access-token')}`
    });

    const params = {
      'name': name,
      'email': email
    };

    return firstValueFrom(this.http.post(this.routeUrl + '/subscribeMaillist',  JSON.stringify(params), { headers }))
      .then((response: any) => {
        if (response.success) {
          return Promise.resolve(true);
        } else if (response.error) {
          return Promise.reject('We hebben je niet op de lijst kunnen plaatsen. Probeer het later nogmaals of met' +
            'een een ander email adres');
        }
      })
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

}
