import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {GlobalEventsManager} from '../global-events-manager';
import {UserService} from '../services/user.service';

@Injectable({ providedIn: 'root'})
export class IsLoggedInGuard implements CanActivate {
  constructor(private globalEventsManager: GlobalEventsManager, private userService: UserService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      return this.userService.getUser().then(() => {
        this.globalEventsManager.showLoggedInNavBar(true);
        return true;
      }, () => {
        this.router.navigate(['/login']);
        return false;
      });
  }
}
