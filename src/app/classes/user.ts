export class User {
  name: string;
  email: string;
  role: string;

  constructor(account: any) {
    this.name = account.name;
    this.email = account.email;
    this.role = account.role;
  }
}
