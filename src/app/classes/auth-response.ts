export class AuthResponse {
  accessToken: string;
  refreshToken: string;
  tokenType: string;

  constructor(access_token, refresh_token, token_type) {
    this.accessToken = access_token;
    this.refreshToken = refresh_token;
    this.tokenType = token_type;
  }
}
