export class Comment {
  name: string;
  comment: string;
  created_at: string;

  constructor(comment: Comment) {
    this.name = comment.name;
    this.comment = comment.comment;
    const date = new Date(comment.created_at);
    this.created_at = this.formatNumber(date.getDay()) + '-' + this.formatNumber(date.getMonth()) + '-'
                      + this.formatNumber(date.getFullYear()) + ' ' + this.formatNumber(date.getHours()) + ':'
                      + this.formatNumber(date.getMinutes());
  }

  formatNumber(dateNumber: number): string {
    if(dateNumber < 10) {
      return '0' + dateNumber;
    }
    else {
      return '' + dateNumber;
    }
  }
}
