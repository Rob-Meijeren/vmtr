export class Event {
  id: number;
  day: number;
  month: number;
  dayNumber: number;
  title: string;
  description: string;
  membersOnly: boolean;
  start: string;
  end: string;

  constructor(event: Event) {
    this.id = event.id;
    this.day = event.day;
    this.month = event.month;
    this.dayNumber = event.dayNumber;
    this.title = event.title;
    this.description = event.description;
    this.membersOnly = event.membersOnly;
    this.start = event.start;
    this.end = event.end;
  }
}
