import {Photo} from './photo';

export class Album {
  id: number;
  name: string;
  description: string;
  public: number;
  photos: Array<Photo>;

  constructor(newAlbum: any) {
    this.id = newAlbum.id;
    this.name = newAlbum.name;
    this.description = newAlbum.description;
    this.public = newAlbum.public;
    this.photos = [];
  }
}

