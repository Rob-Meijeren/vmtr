export class DownloadRoute {
  body: string;

  constructor(file: any) {
    this.body = file._body;
  }
}
