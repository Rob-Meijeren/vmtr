export class UploadFile {
  filename: string;
  type: string;
  base64String: string;

  constructor(UploadedFile: any) {
    this.filename = UploadedFile.name;
    this.type = UploadedFile.type;
  }

  xmlToBase64(file: File, callback: Function, componentRef: any): void {
    const myReader: FileReader = new FileReader();

    myReader.onloadend = () => {
      this.base64String = btoa(myReader.result.toString());
      callback(this, componentRef);
    };
    myReader.readAsText(file, 'utf-8');
  }

  imageToBase64(file: File, callback: Function, componentRef: any): void {
    const myReader: FileReader = new FileReader();

    myReader.onloadend = () => {
      this.base64String = myReader.result.toString();
      callback(this, componentRef);
    };
    myReader.readAsDataURL(file);
  }
}
