import {environment} from '../../environments/environment';

export class Photo {
  id: number;
  path: string;

  constructor(newPhoto: Photo) {
    this.id = newPhoto.id;
    if (newPhoto.path.startsWith('/')) {
      this.path = environment.api_base + newPhoto.path;
    } else {
      this.path = environment.api_base + '/' + newPhoto.path;
    }
  }
}
