export class Route {
  name: string;
  centerLatitude: number;
  centerLongitude: number;
  filename: string;
  public: number;

  constructor(route: Route) {
    this.name = route.name;
    this.filename = route.filename;
    this.centerLatitude = route.centerLatitude;
    this.centerLongitude = route.centerLongitude;
    this.public = route.public;
  }
}
