import {Component, OnInit, ViewChild} from '@angular/core';
import {AlbumService} from '../../services/album.service';
import {UploadPhotoComponent} from '../../components/upload-photo/upload-photo.component';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';

@Component({
  selector: 'app-add-album',
  templateUrl: './add-album.component.html',
  styleUrls: ['./add-album.component.scss']
})
export class AddAlbumComponent implements OnInit {
  @ViewChild('uploadPhoto', { static: false }) uploadPhoto: UploadPhotoComponent;
  success: Array<string>;
  error: Array<string>;
  uploading: boolean;

  albumForm: FormGroup;

  constructor(private albumService: AlbumService, private fb: FormBuilder) {
    this.uploading = false;
    this.success = [];
    this.error = [];
    this.albumForm = this.fb.group({
      name: new FormControl(''),
      description: new FormControl(''),
    });
  }

  ngOnInit() {
  }

  createAlbum() {
    this.uploading = true;
    this.albumService.createAlbum(this.albumForm.value.name, this.albumForm.value.description).then(response => {
      this.uploadPhoto.uploadFiles(response.albumID);
    }, error => {
      this.error = error;
      setTimeout(() => {
        this.error.splice(this.error.indexOf(error), 1);
      }, 5000);
    }).then(() => this.uploading = false);
  }

  onPhotoUploadSuccess(result: string) {
    this.success.push(result);
    setTimeout(() => {
      this.success.splice(this.success.indexOf(result), 1);
    }, 5000);
  }

  onPhotoUploadError(error: string) {
    this.error.push(error);
    setTimeout(() => {
      this.error.splice(this.error.indexOf(error), 1);
    }, 5000);
  }

}
