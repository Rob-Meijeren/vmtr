import { Component, OnInit } from '@angular/core';
import {ContactService} from '../../services/contact.service';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-become-member',
  templateUrl: './become-member.component.html',
  styleUrls: ['./become-member.component.scss']
})
export class BecomeMemberComponent implements OnInit {
  becomeMemberForm: FormGroup;
  success: string;
  error: string;

  constructor(private contactService: ContactService, private fb: FormBuilder) {
    this.success = '';
    this.error = '';

    this.becomeMemberForm = this.fb.group({
      name: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      phone: new FormControl(''),
    });
  }

  ngOnInit() {}

  signup() {
    this.contactService.becomeMember(this.becomeMemberForm.value.name, this.becomeMemberForm.value.email,
                                     this.becomeMemberForm.value.phone).then(success => {
      this.success = success;
    }, error => {
      this.error = error;
    });
  }

}
