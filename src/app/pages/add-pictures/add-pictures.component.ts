import {Component, OnInit, ViewChild} from '@angular/core';
import {Album} from '../../classes/album';
import {AlbumService} from '../../services/album.service';
import {UploadPhotoComponent} from '../../components/upload-photo/upload-photo.component';
import {Photo} from '../../classes/photo';
import {PictureService} from '../../services/picture.service';

@Component({
  selector: 'app-add-pictures',
  templateUrl: './add-pictures.component.html',
  styleUrls: ['./add-pictures.component.scss']
})
export class AddPicturesComponent implements OnInit {
  @ViewChild('uploadPhoto', { static: false }) uploadPhoto: UploadPhotoComponent;
  albums: Array<Album>;
  selectedAlbum: Album;
  albumPictures: Array<Photo>;
  loading: boolean;
  error: Array<string>;
  uploadError: string;
  success: Array<string>;

  constructor(private albumService: AlbumService, private pictureService: PictureService) {
    this.loading = false;
    this.success = [];
    this.error = [];
  }

  ngOnInit() {
    this.loading = true;
    this.albumService.getAllAlbums().then(albums => {
      this.albums = albums;
    }, error => {
      this.error = error;
    }).then( () => {
      this.loading = false;
    });
  }

  getPhotosOfAlbum() {
    this.loading = true;
    this.pictureService.getPhotosOfAlbum(this.selectedAlbum.id).then(photos => {
      this.albumPictures = photos;
    }, error => {
      this.error = error;
    }).then( () => {
      this.loading = false;
    });
  }

  addPhotosToAlbum() {
    if (this.uploadPhoto.photosToUpload.length > 0) {
      this.uploadPhoto.uploadFiles(this.selectedAlbum.id);
    } else {
      this.uploadError = 'Er zijn geen foto\'s geselecteerd om te uploaden.';
      setTimeout(() => {
        this.uploadError = '';
      }, 5000);
    }
  }

  onPhotoUploadSuccess(result: string) {
    this.success.push(result);
    setTimeout(() => {
      this.success.splice(this.success.indexOf(result), 1);
    }, 5000);
    this.getPhotosOfAlbum();
  }

  onPhotoUploadError(error: string) {
    this.error.push(error);
    setTimeout(() => {
      this.error.splice(this.error.indexOf(error), 1);
    }, 5000);

  }

  deletePhoto(photo: Photo) {
    this.loading = true;
    this.pictureService.deletePicture(photo).then(() => {
      this.albumPictures.splice(this.albumPictures.findIndex(i => i.id === photo.id), 1);
      this.loading = false;
    });
  }

}
