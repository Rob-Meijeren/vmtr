import { Component, OnInit } from '@angular/core';
import {Event} from '../../classes/event';
import {EventService} from '../../services/event.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  nextEvent: Event;

  constructor(private eventService: EventService) {}

  ngOnInit() {
    this.eventService.getNextEvent().then(event => {
      this.nextEvent = event;
    }, () => {});
  }

}
