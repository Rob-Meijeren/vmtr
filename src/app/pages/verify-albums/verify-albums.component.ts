import { Component, OnInit } from '@angular/core';
import {AlbumService} from '../../services/album.service';
import {Album} from '../../classes/album';
import {PictureService} from '../../services/picture.service';
import { Photo } from 'src/app/classes/photo';
import { ImageItem } from '@ngx-gallery/core';

@Component({
  selector: 'app-verify-albums',
  templateUrl: './verify-albums.component.html',
  styleUrls: ['./verify-albums.component.scss']
})
export class VerifyAlbumsComponent implements OnInit {
  public noWrapSlides = false;
  public myInterval = 5000;
  albums: Array<Album>;
  images: any[];
  error: string;

  constructor(private albumService: AlbumService, private pictureService: PictureService) {
    this.albums = [];
    this.images = [];
    this.error = '';
  }

  ngOnInit() {
    this.albumService.getAllAlbums().then(albums => {
      this.albums = albums;
      this.albums.forEach((album: Album, albumIdx: number) => {
        this.pictureService.getPhotosOfAlbum(album.id).then(photos => {
          album.photos = photos;
          const photoArray = []; 
          photos.forEach((photo: Photo) => {
            const imageItem = new ImageItem({ src: photo.path, thumb: photo.path });
            photoArray.push(imageItem);
          });
          this.images[albumIdx] = photoArray;
        });
      });
    }, error => {
      this.error = error;
    });
  }

  markAsPublic(album: Album) {
    this.albumService.markAsPublic(album);
  }

  markAsPrivate(album: Album) {
    this.albumService.markAsPrivate(album);
  }
}
