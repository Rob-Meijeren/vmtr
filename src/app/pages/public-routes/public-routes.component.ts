import { Component, OnInit } from '@angular/core';
import {RouteService} from '../../services/route.service';
import {Route} from '../../classes/route';
import { MatDialog } from '@angular/material/dialog';
import { RouteDownloadModalComponent } from 'src/app/components/route-download-modal/route-download-modal.component';

export interface RouteView {
  route: Route;
  hasGPX: boolean;
  hasKML: boolean;
  hasITN: boolean;
}

@Component({
  selector: 'app-public-routes',
  templateUrl: './public-routes.component.html',
  styleUrls: ['./public-routes.component.scss']
})
export class PublicRoutesComponent implements OnInit {
  routes: Array<RouteView>;
  error: string;

  constructor(private routeService: RouteService, private dialog: MatDialog) {
    this.routes = [];
  }

  ngOnInit() {
    this.routeService.getPublicRoutes().then((routes: Route[]) => {
      routes.forEach(( route: Route) => {
        this.routeService.checkFilePresence(route.filename).then(presentArray => {
          this.routes.push({
            route,
            hasGPX: presentArray.gpx,
            hasKML: presentArray.kml,
            hasITN: presentArray.itn,
          });
        });
      });
    }, error => {
      this.error = error;
    });
  }

  openRouteInModal( route: Route, extension: string ) {
    this.dialog.open( RouteDownloadModalComponent, {
      maxWidth: '500px',
      maxHeight: '600px',
      data: {
        route,
        downloadExtension: extension
      }
    });
  }

}
