import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyRouteComponent } from './verify-route.component';

describe('VerifyRouteComponent', () => {
  let component: VerifyRouteComponent;
  let fixture: ComponentFixture<VerifyRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
