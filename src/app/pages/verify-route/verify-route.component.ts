import { Component, OnInit } from '@angular/core';
import {RouteService} from '../../services/route.service';
import { Route } from 'src/app/classes/route';

@Component({
  selector: 'app-verify-route',
  templateUrl: './verify-route.component.html',
  styleUrls: ['./verify-route.component.scss']
})
export class VerifyRouteComponent implements OnInit {
  routes: Array<Route>;
  error: string;

  constructor(private routeService: RouteService) {
    this.routes = [];
  }

  ngOnInit() {
    this.routeService.getAllRoutes().then(routes => {
      this.routes = routes;
    }, error => {
      this.error = error;
    });
  }

  markAsPrivate(route: Route) {
    this.routeService.markAsPrivate(route);
  }

  markAsPublic(route: Route) {
    this.routeService.markAsPublic(route);
  }
}
