/// <reference types="@types/google.maps" />
// we need the line above to make sure the compiler doesn't complain about the google references in this file

import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {RouteService} from '../../services/route.service';
import {Route} from '../../classes/route';
import {ActivatedRoute} from '@angular/router';
import {environment} from '../../../environments/environment';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-route-detail',
  templateUrl: './route-detail.component.html',
  styleUrls: ['./route-detail.component.scss']
})
export class RouteDetailComponent implements OnInit {
  routeUrl: string;
  route: Route;
  error: string;
  hasGPX: boolean;
  hasKML: boolean;
  hasITN: boolean;
  fileDownload: string;
  routeName: string;

  @ViewChild('map', { read: ElementRef, static: false }) mapElement: ElementRef;

  map: google.maps.Map;
  kmlLayer: google.maps.KmlLayer;

  constructor(private routeService: RouteService, private urlRoute: ActivatedRoute, private userService: UserService) {
    this.routeUrl = environment.api_url + '/route/downloadKML/';
    this.hasGPX = false;
    this.hasKML = false;
    this.hasITN = false;
    this.fileDownload = '';
  }

  ngOnInit() {
    this.urlRoute.params.subscribe(params => {
      this.routeService.getSpecificRoute(params.name).then(route => {
        this.route = route;
        this.routeService.checkFilePresence(this.route.filename).then(presentArray => {
          this.hasGPX = presentArray.gpx;
          this.hasKML = presentArray.kml;
          this.hasITN = presentArray.itn;
        });
        const mapProperties = {
          center: new google.maps.LatLng(route.centerLatitude, route.centerLongitude),
          zoom: 9,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
        } as google.maps.MapOptions;
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapProperties);
        this.kmlLayer = new google.maps.KmlLayer({
          map: this.map,
          url: `${this.routeUrl}${this.route.filename}`
        })
      });
    });
  }

  checkForUser(): Promise<boolean> {
    return this.userService.getUser().then(() => {
      return Promise.resolve(true);
    }, () => {
      return Promise.reject(false);
    });
  }

  downloadGPX() {
    this.checkForUser().then(() => {
      this.routeService.downloadGPX(this.route.filename);
    }, () => {
      this.fileDownload = 'gpx';
    });
  }

  downloadKML() {
    this.checkForUser().then(result => {
      this.routeService.downloadKML(this.route.filename);
    }, () => {
      this.fileDownload = 'kml';
    });
  }

  downloadITN() {
    this.checkForUser().then(result => {
      this.routeService.downloadITN(this.route.filename);
    }, () => {
      this.fileDownload = 'itn';
    });
  }

}
