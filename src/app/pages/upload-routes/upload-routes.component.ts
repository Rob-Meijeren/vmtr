import { Component, OnInit } from '@angular/core';
import {UploadFile} from '../../classes/upload-file';
import {Route} from '../../classes/route';
import {RouteService} from '../../services/route.service';

@Component({
  selector: 'app-upload-routes',
  templateUrl: './upload-routes.component.html',
  styleUrls: ['./upload-routes.component.scss']
})
export class UploadRoutesComponent implements OnInit {
  routesToUpload: Array<any>;
  success: Array<string>;
  error: Array<string>;

  constructor(private routeService: RouteService) {
    this.routesToUpload = [];
    this.success = [];
    this.error = [];
  }

  ngOnInit() {}

  addFiles(event) {
    this.routesToUpload = Array.from(event.target.files);
  }

  uploadFiles() {
    this.routesToUpload.forEach(route => {
      const metaRoute = new UploadFile(route);
      metaRoute.xmlToBase64(route, this.uploadIndividualFile, this);
    });
  }

  uploadIndividualFile(route: UploadFile, componentRef: UploadRoutesComponent) {
    componentRef.routeService.uploadRoute(route).then(result => {
      componentRef.success.push(result);
      setTimeout(() => {
        componentRef.success.splice(componentRef.success.indexOf(result), 1);
      }, 5000);
    }, error => {
      componentRef.error.push(error);
      setTimeout(() => {
        componentRef.error.splice(componentRef.error.indexOf(error), 1);
      }, 5000);
    });
  }

}
