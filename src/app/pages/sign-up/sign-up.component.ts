import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  success: string;
  error: string;

  public newUserForm: FormGroup;

  constructor(private userService: UserService, private fb: FormBuilder) {
    this.newUserForm = this.fb.group({
      name: new FormControl(''),
      email: new FormControl('', Validators.email),
      password: new FormControl(''),
    });
  }

  ngOnInit() {
  }

  registerUser() {
    this.userService.register(this.newUserForm.value.name, this.newUserForm.value.email, this.newUserForm.value.password).then(result => {
      if(result) {
        this.success = 'De gebruiker is aangemaakt.';
      } else {
        this.error = 'Er is iets fout gegaan.';
      }
    });
  }

}
