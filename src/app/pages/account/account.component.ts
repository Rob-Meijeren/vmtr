import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user.service';
import {User} from '../../classes/user';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  accountSuccess: string;
  accountFailure: string;
  passwordSuccess: string;
  passwordFailure: string;

  passwordForm: FormGroup;
  accountForm: FormGroup;

  constructor(private userService: UserService, private fb: FormBuilder) {
    this.accountForm = this.fb.group({
      name: new FormControl(''),
      email: new FormControl('', Validators.email),
      role: new FormControl(''),
    });

    this.passwordForm = this.fb.group({
      oldPassword: new FormControl('', Validators.required),
      newPassword: new FormControl('', Validators.required),
      confirmPassword: new FormControl('', Validators.required),
    });
  }

  ngOnInit() {
    this.userService.getUser().then((user: User) => {
      this.accountForm.get('name').setValue(user.name);
      this.accountForm.get('email').setValue(user.email);
      this.accountForm.get('role').setValue(user.role);
    });
  }

  updateAccount() {
    const user: User = {
      name: this.accountForm.get('name').value,
      email: this.accountForm.get('email').value,
      role: this.accountForm.get('role').value,
    };
    this.userService.updateUser(user).then(result => {
      this.accountSuccess = result;
    }, error => {
      this.accountFailure = error;
    }).then(() => {
      setTimeout(() => {
        this.accountSuccess = '';
        this.accountFailure = '';
      }, 5000);
    });
  }

  changePassword() {
    this.userService.updatePassword(this.passwordForm.get('oldPassword').value, this.passwordForm.get('newPassword').value).then(result => {
      this.passwordSuccess = result;
    }, error => {
      this.passwordFailure = error;
    }).then(() => {
      setTimeout(() => {
        this.passwordFailure = '';
        this.passwordSuccess = '';
      }, 5000);
    });
  }

}
