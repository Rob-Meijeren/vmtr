import { Component, OnInit } from '@angular/core';
import {Route} from '../../classes/route';
import {RouteService} from '../../services/route.service';

@Component({
  selector: 'app-member-routes',
  templateUrl: './member-routes.component.html',
  styleUrls: ['./member-routes.component.scss']
})
export class MemberRoutesComponent implements OnInit {
  routes: Array<Route>;
  error: string;

  constructor(private routeService: RouteService) {
    this.routes = [];
  }

  ngOnInit() {
    this.routeService.getAllRoutes().then(routes => {
      this.routes = routes;
    }, error => {
      this.error = error;
    });
  }

}
