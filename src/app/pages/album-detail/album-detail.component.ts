import { Component, OnInit, ViewChild } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AlbumService} from '../../services/album.service';
import {PictureService} from '../../services/picture.service';
import {Album} from '../../classes/album';
import { Photo } from 'src/app/classes/photo';
import { GalleryItem, ImageItem, GalleryComponent } from '@ngx-gallery/core';

@Component({
  selector: 'app-album-detail',
  templateUrl: './album-detail.component.html',
  styleUrls: ['./album-detail.component.scss']
})
export class AlbumDetailComponent implements OnInit {
  album: Album;
  @ViewChild(GalleryComponent, { static: false }) gallery: GalleryComponent;

  constructor(private urlRoute: ActivatedRoute, private albumService: AlbumService, private pictureService: PictureService) {
    this.album = new Album({});
  }

  ngOnInit() {
    this.urlRoute.params.subscribe(params => {
      this.albumService.getSpecificAlbum(params['id']).then(album => {
        this.album = album;
        this.pictureService.getPhotosOfAlbum(this.album.id).then((photos: Photo[]) => {
          this.album.photos = photos;
          photos.forEach((photo: Photo) => {
            this.gallery.addImage({ src: photo.path, thumb: photo.path });
          });
        });
      });
    });
  }

}
