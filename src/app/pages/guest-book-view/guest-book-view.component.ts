import { Component, OnInit } from '@angular/core';
import {GuestBookService} from '../../services/guest-book.service';
import {Comment} from '../../classes/comment';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-guest-book-view',
  templateUrl: './guest-book-view.component.html',
  styleUrls: ['./guest-book-view.component.scss']
})
export class GuestBookViewComponent implements OnInit {
  comments: Array<Comment>;
  error: string;
  guestbookForm: FormGroup;

  constructor(private guestbookService: GuestBookService, private fb: FormBuilder) {
    this.guestbookForm = this.fb.group({
      name: new FormControl(''),
      message: new FormControl('', Validators.required),
    })
  }

  ngOnInit() {
    this.guestbookService.getAllComments().then(comments => {
      this.comments = comments;
    }, error => {
      this.error = error;
    });
  }

  placeComment() {
    this.guestbookService.placeComment(this.guestbookForm.value.name, this.guestbookForm.value.message).then(comment => {
      this.comments.unshift(comment);
    }, error => {
      this.error = error;
    });
  }

}
