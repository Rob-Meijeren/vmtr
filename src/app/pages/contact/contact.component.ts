import { Component, OnInit } from '@angular/core';
import {ContactService} from '../../services/contact.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  contactForm: FormGroup;
  success: string;
  error: string;

  constructor(private contactService: ContactService, private fb: FormBuilder) {
    this.contactForm = this.fb.group({
      name: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      phone: new FormControl(''),
      subject: new FormControl('', Validators.required),
      message: new FormControl('', Validators.required),
    });
  }

  ngOnInit() {
  }

  sendForm() {
    this.contactService.sendForm(this.contactForm.value.name, this.contactForm.value.email,
                                 this.contactForm.value.phone, this.contactForm.value.subject,
                                 this.contactForm.value.message).then(result => {
      this.success = result;
    }, error => {
      this.error = error;
    });
  }

}
