import { Component, OnInit } from '@angular/core';
import {Album} from '../../classes/album';
import {AlbumService} from '../../services/album.service';
import {PictureService} from '../../services/picture.service';
import { Router } from '@angular/router';
import { ImageItem } from '@ngx-gallery/core';
import { Photo } from 'src/app/classes/photo';

@Component({
  selector: 'app-gallery-view',
  templateUrl: './gallery-view.component.html',
  styleUrls: ['./gallery-view.component.scss']
})
export class GalleryViewComponent implements OnInit {
  public noWrapSlides = false;
  public myInterval = 5000;
  albums: Array<Album>;
  error: string;
  images: any[];

  constructor(private albumService: AlbumService, private router: Router, private pictureService: PictureService) {
    this.albums = [];
    this.images = [];
    this.error = '';
  }

  ngOnInit() {
    this.albumService.getPublicAlbums().then(albums => {
      this.albums = albums;
      this.albums.forEach((album: Album, albumIdx: number) => {
        this.pictureService.getPhotosOfAlbum(album.id).then((photos: Photo[]) => {
          album.photos = photos;
          const photoArray = [];
          photos.forEach((photo: Photo) => {
            const imageItem = new ImageItem({ src: photo.path, thumb: photo.path });
            photoArray.push(imageItem);
          });
          this.images[albumIdx] = photoArray;
        });
      });
    }, error => {
      this.error = error;
    });
  }

  navigateToAlbum(albumId: number) {
    this.router.navigate([`/album/${albumId}`]);
  }

}
