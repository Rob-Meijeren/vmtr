import { Component, OnInit } from '@angular/core';
import {ContactService} from '../../services/contact.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-tent',
  templateUrl: './tent.component.html',
  styleUrls: ['./tent.component.scss']
})
export class TentComponent implements OnInit {
  success: string;
  error: string;
  tentForm: FormGroup;

  constructor(
    private contactService: ContactService,
    private fb: FormBuilder,
  ) {
    this.tentForm = this.fb.group({
      name: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      phone: new FormControl(''),
      message: new FormControl('', Validators.required),
    });
  }

  ngOnInit() {
  }

  book() {
    this.contactService.requestTent(this.tentForm.controls.name.value, this.tentForm.controls.email.value,
                                    this.tentForm.controls.phone.value, this.tentForm.controls.message.value)
      .then(success => {
        this.success = success;
      }, error => {
        this.error = error;
      }).then(() => {
        setTimeout(() => {
          this.success = '';
          this.error = '';
        }, 10000);
      });
  }
}
