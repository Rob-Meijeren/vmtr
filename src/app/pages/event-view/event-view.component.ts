import { Component, OnInit } from '@angular/core';
import {Event} from '../../classes/event';
import {EventService} from '../../services/event.service';
import { User } from 'src/app/classes/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-event-view',
  templateUrl: './event-view.component.html',
  styleUrls: ['./event-view.component.scss']
})
export class EventViewComponent implements OnInit {
  events: Array<Event>;
  loggedInUser: User;

  constructor(private eventService: EventService, private userService: UserService) {
    this.events = [];
    this.loggedInUser = null;
  }

  ngOnInit() {
    this.userService.getUser().then((loggedInUser: User) => {
      this.loggedInUser = loggedInUser;
      if (loggedInUser && loggedInUser.role === 'Admin') {
        this.getAllEvents();
      } else {
        this.getUpcomingEvents();
      }
    }).catch(() => {
      this.getUpcomingEvents();
    });
  }

  getUpcomingEvents() {
    this.eventService.getUpcomingEvents().then(events => {
      this.events = events;
    }, () => {});
  }

  getAllEvents() {
    this.eventService.getAllEvents().then(events => {
      this.events = events;
    }, () => {});
  }

}
