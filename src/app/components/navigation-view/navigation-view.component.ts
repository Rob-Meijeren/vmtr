import { Component, OnDestroy, OnInit } from '@angular/core';
import {GlobalEventsManager} from '../../global-events-manager';
import {Router} from '@angular/router';
import {UserService} from '../../services/user.service';
import {User} from '../../classes/user';
import { MediaObserver } from '@angular/flex-layout';
import { Subject, takeUntil } from 'rxjs';

export interface INavItem {
  name: string;
  routeLink?: string;
  isSubMenu?: boolean;
  menuName?: string;
  subItems?: INavItem[];
  action?: string;
}

@Component({
  selector: 'app-navigation-view',
  templateUrl: './navigation-view.component.html',
  styleUrls: ['./navigation-view.component.scss']
})
export class NavigationViewComponent implements OnInit, OnDestroy {
  isCollapsed: boolean;
  showLoggedInNavBar = false;
  user: User;
  showCollapsedNavbar = false;
  private stop$ = new Subject<void>();

  publicRoutes: INavItem[] = [
    { name: 'Home', routeLink: '/' },
    { name: 'Evenementen', routeLink: '/evenementen' },
    { name: 'Geschiedenis', routeLink: '/overVMTR' },
    { name: 'Routes', routeLink: '/routes' },
    { name: 'Lid worden', routeLink: '/lidworden' },
    { name: 'Foto\'s', routeLink: '/gallerij' },
    { name: 'Gastenboek', routeLink: '/gastenboek' },
    { name: 'Contact', routeLink: '/contact' },
    { name: 'Login', routeLink: '/login' },
  ];

  loggedInroutes: INavItem[] = [
    { name: 'Home', routeLink: '/' },
    { name: 'Evenementen', routeLink: '/evenementen' },
    { name: 'Account', routeLink: '/account' },
    { name: 'Albums maken/bewerken', isSubMenu: true, menuName: 'albumMenu', subItems: [
      { name: 'Maak een Album', routeLink: '/leden/voegAlbumToe' },
      { name: 'Voeg Foto\'s aan een album toe', routeLink: '/leden/voegFotosToeAanAlbum' },
    ] },
    { name: 'Routes', isSubMenu: true, menuName: 'routeMenu', subItems: [
      { name: 'Bekijk alle routes', routeLink: '/leden/routes' },
      { name: 'Voeg een route toe', routeLink: '/leden/uploadRoutes' },
    ] },
    { name: 'Loguit', action: 'logout' },
  ];

  adminRoutes: INavItem[] = [
    { name: 'Home', routeLink: '/' },
    { name: 'Evenementen', routeLink: '/evenementen' },
    { name: 'Account', isSubMenu: true, menuName: 'accountMenu', subItems: [
      { name: 'Bekijk Account', routeLink: '/account' },
      { name: 'Registreer nieuw account', routeLink: '/registreer' },
    ] },
    { name: 'Albums maken/bewerken', isSubMenu: true, menuName: 'albumMenu', subItems: [
      { name: 'Maak een Album', routeLink: '/leden/voegAlbumToe' },
      { name: 'Voeg Foto\'s aan een album toe', routeLink: '/leden/voegFotosToeAanAlbum' },
      { name: 'Maak albums beschikbaar', routeLink: '/albums/public' },
    ] },
    { name: 'Routes', isSubMenu: true, menuName: 'routeMenu', subItems: [
      { name: 'Bekijk alle routes', routeLink: '/leden/routes' },
      { name: 'Voeg een route toe', routeLink: '/leden/uploadRoutes' },
      { name: 'Maak routes beschikbaar', routeLink: '/routes/public' },
    ] },
    { name: 'Loguit', action: 'logout' },
  ];

  constructor(
    private globalEventsManager: GlobalEventsManager,
    private router: Router,
    private userService: UserService,
    private media: MediaObserver,
  ) {
    this.isCollapsed = true;
    this.globalEventsManager.showNavBarEmitter.subscribe(mode => {
      // mode will be null the first time it is created, so you need to ignore it when it is null
      if (mode !== null) {
        this.showLoggedInNavBar = mode;
        if (this.showLoggedInNavBar) {
          this.setUser();
        }
      }
    });
  }

  ngOnInit() {
    this.setUser();

    this.media.asObservable().pipe(
      takeUntil(this.stop$),
    ).subscribe((mediaChange) => {
      const screenSize = mediaChange[0];
      switch (screenSize.mqAlias) {
        case 'xs':
        case 'sm':
          this.showCollapsedNavbar = true;
          break;
        default:
          this.showCollapsedNavbar = false;
          break;
      }
    });
  }

  ngOnDestroy() {
    this.stop$.next();
  }

  setUser() {
    this.userService.getUser().then(user => {
      this.user = user;
      if (!this.showLoggedInNavBar) {
        this.showLoggedInNavBar = true;
      }
    });
  }

  logout() {
    this.userService.logout().then(() => {
      this.showLoggedInNavBar = false;
      this.user = null;
      this.router.navigate(['/']);
    });
  }

  handleRouteAction(route: INavItem) {
    if (route.action === 'logout') {
      this.logout();
    }
  }

}
