import { Component, Input, OnInit } from '@angular/core';
import { INavItem } from '../navigation-view.component';

@Component({
  selector: 'app-sub-nav-item',
  templateUrl: './sub-nav-item.component.html',
  styleUrls: ['./sub-nav-item.component.scss']
})
export class SubNavItemComponent implements OnInit {

  @Input()
  public route: INavItem;

  constructor() { }

  ngOnInit(): void {
  }

}
