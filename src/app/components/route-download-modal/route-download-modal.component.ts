import {Component, OnInit, Inject} from '@angular/core';
import {Route} from '../../classes/route';
import {RouteService} from '../../services/route.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

export interface DialogData {
  route: Route;
  downloadExtension: string;
}

@Component({
  selector: 'app-route-download-modal',
  templateUrl: './route-download-modal.component.html',
  styleUrls: ['./route-download-modal.component.scss']
})
export class RouteDownloadModalComponent implements OnInit {
  private downloadExtension: string;
  public downloadForm: FormGroup;
  public route: Route;

  constructor(
    private routeService: RouteService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<RouteDownloadModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
      this.route = data.route;
      this.downloadExtension = data.downloadExtension;

      this.downloadForm = this.fb.group({
        name: new FormControl( '', Validators.required ),
        email: new FormControl( '', Validators.required ),
        errorString: new FormControl( '' ),
      } );
  }

  ngOnInit() {
  }

  downloadRoute() {
    this.routeService.subscribeMaillist(this.downloadForm.controls.name.value, this.downloadForm.controls.email.value).then(() => {
      let downloadPromise;
      if (this.downloadExtension === 'gpx') {
        downloadPromise = this.routeService.downloadGPX(this.route.filename);
      } else if (this.downloadExtension === 'kml') {
        downloadPromise = this.routeService.downloadKML(this.route.filename);
      } else if (this.downloadExtension === 'itn') {
        downloadPromise = this.routeService.downloadITN(this.route.filename);
      }

      downloadPromise.then(() => {
        this.dialogRef.close();
      });
    }, error => {
      this.downloadForm.controls.error.setValue( error );
    });
  }

  close() {
    this.dialogRef.close();
  }

}
