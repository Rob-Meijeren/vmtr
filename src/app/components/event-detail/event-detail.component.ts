import {Component, Input, OnInit, OnChanges, SimpleChanges} from '@angular/core';
import {Event} from '../../classes/event';
import * as moment from 'moment';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { EventService } from 'src/app/services/event.service';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.scss']
})
export class EventDetailComponent implements OnInit, OnChanges {
  @Input() event: Event;
  @Input() editable: boolean;

  public dateGroup: FormGroup;

  constructor(private fb: FormBuilder, private eventService: EventService) {
    this.dateGroup = fb.group({
      start: new FormControl(''),
      end: new FormControl(''),
    });
  }

  ngOnInit() {
    if (this.event) {
      this.setEventDateControls();
    }
  }

  ngOnChanges() {
    if (this.event) {
      this.setEventDateControls();
    }
  }

  setEventDateControls() {
    this.dateGroup.get('start').setValue(moment(this.event.start, 'DD-MM-YYYY'));
    this.dateGroup.get('end').setValue(moment(this.event.end, 'DD-MM-YYYY'));
  }

  changeDate() {
    const event: any = {
      id: this.event.id,
      start: this.dateGroup.get('start').value.format('YYYY-MM-DD'),
      end: this.dateGroup.get('end').value.format('YYYY-MM-DD')
    };
    this.eventService.updateDates(event);
  }

}
