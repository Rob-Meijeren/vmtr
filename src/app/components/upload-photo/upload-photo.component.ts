import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {UploadFile} from '../../classes/upload-file';
import {PictureService} from '../../services/picture.service';

@Component({
  selector: 'app-upload-photo',
  templateUrl: './upload-photo.component.html',
  styleUrls: ['./upload-photo.component.scss']
})
export class UploadPhotoComponent implements OnInit {
  photosToUpload: Array<any>;
  albumId: string;
  @Output() photoUploadSuccess: EventEmitter<string> = new EventEmitter<string>();
  @Output() photoUploadError: EventEmitter<string> = new EventEmitter<string>();

  constructor(private photoService: PictureService) {
    this.photosToUpload = [];
  }

  ngOnInit() {
  }

  addFiles(event) {
      this.photosToUpload = Array.from(event.srcElement.files);
  }


  uploadFiles(albumId: string|number) {
    this.albumId = '' + albumId;
    this.photosToUpload.forEach(image => {
      const metaImage = new UploadFile(image);
      metaImage.imageToBase64(image, this.uploadIndividualFile, this);
    });
  }

  uploadIndividualFile(image: UploadFile, componentRef: UploadPhotoComponent) {
    componentRef.photoService.uploadPicture(componentRef.albumId, image).then(result => {
        componentRef.photoUploadSuccess.emit(result);
    }, error => {
      componentRef.photoUploadError.emit(error);
    });
  }

}
