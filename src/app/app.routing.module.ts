import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { HistoryComponent } from './pages/history/history.component';
import { TentComponent } from './pages/tent/tent.component';
import { PublicRoutesComponent } from './pages/public-routes/public-routes.component';
import { RouteDetailComponent } from './pages/route-detail/route-detail.component';
import { MemberRoutesComponent } from './pages/member-routes/member-routes.component';
import { UploadRoutesComponent } from './pages/upload-routes/upload-routes.component';
import { LoginComponent } from './pages/login/login.component';
import { BecomeMemberComponent } from './pages/become-member/become-member.component';
import { SignUpComponent } from './pages/sign-up/sign-up.component';
import { AccountComponent } from './pages/account/account.component';
import { GuestBookViewComponent } from './pages/guest-book-view/guest-book-view.component';
import { ContactComponent } from './pages/contact/contact.component';
import { GalleryViewComponent } from './pages/gallery-view/gallery-view.component';
import { AlbumDetailComponent } from './pages/album-detail/album-detail.component';
import { AddAlbumComponent } from './pages/add-album/add-album.component';
import { AddPicturesComponent } from './pages/add-pictures/add-pictures.component';
import { VerifyAlbumsComponent } from './pages/verify-albums/verify-albums.component';
import { EventViewComponent } from './pages/event-view/event-view.component';
import { IsLoggedInGuard } from './guards/is-logged-in.guard';
import { AdminGuard } from './guards/admin.guard';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'overVMTR', component: HistoryComponent },
  { path: 'tent', component: TentComponent },
  { path: 'routes', component: PublicRoutesComponent },
  { path: 'route/:name', component: RouteDetailComponent },
  { path: 'leden/routes', component: MemberRoutesComponent, canActivate: [IsLoggedInGuard] },
  { path: 'leden/uploadRoutes', component: UploadRoutesComponent, canActivate: [IsLoggedInGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'lidworden', component: BecomeMemberComponent },
  { path: 'registreer', component: SignUpComponent },
  { path: 'account', component: AccountComponent, canActivate: [IsLoggedInGuard] },
  { path: 'gastenboek', component: GuestBookViewComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'gallerij', component: GalleryViewComponent },
  { path: 'album/:id', component: AlbumDetailComponent },
  { path: 'leden/voegAlbumToe', component: AddAlbumComponent, canActivate: [IsLoggedInGuard] },
  { path: 'leden/voegFotosToeAanAlbum', component: AddPicturesComponent, canActivate: [IsLoggedInGuard] },
  { path: 'albums/public', component: VerifyAlbumsComponent, canActivate: [AdminGuard] },
  { path: 'evenementen', component: EventViewComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  declarations: [],
  exports: [
    RouterModule
  ]
})
export class AppRouterModule { }