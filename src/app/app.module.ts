import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {RouterModule} from '@angular/router';
import {GlobalEventsManager} from './global-events-manager';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FlexLayoutModule } from '@angular/flex-layout';
import { GalleryModule } from '@ngx-gallery/core';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatIconModule } from '@angular/material/icon';

import { HomeComponent } from './pages/home/home.component';
import { HistoryComponent } from './pages/history/history.component';
import { TentComponent } from './pages/tent/tent.component';
import { PublicRoutesComponent } from './pages/public-routes/public-routes.component';
import { RouteDetailComponent } from './pages/route-detail/route-detail.component';
import { MemberRoutesComponent } from './pages/member-routes/member-routes.component';
import { UploadRoutesComponent } from './pages/upload-routes/upload-routes.component';
import { LoginComponent } from './pages/login/login.component';
import { BecomeMemberComponent } from './pages/become-member/become-member.component';
import { SignUpComponent } from './pages/sign-up/sign-up.component';
import { AccountComponent } from './pages/account/account.component';
import { GuestBookViewComponent } from './pages/guest-book-view/guest-book-view.component';
import { ContactComponent } from './pages/contact/contact.component';
import { GalleryViewComponent } from './pages/gallery-view/gallery-view.component';
import { AlbumDetailComponent } from './pages/album-detail/album-detail.component';
import { AddAlbumComponent } from './pages/add-album/add-album.component';
import { AddPicturesComponent } from './pages/add-pictures/add-pictures.component';
import { VerifyAlbumsComponent } from './pages/verify-albums/verify-albums.component';
import { EventViewComponent } from './pages/event-view/event-view.component';
import { EventDetailComponent } from './components/event-detail/event-detail.component';
import { NavigationViewComponent } from './components/navigation-view/navigation-view.component';
import { RouteDownloadModalComponent } from './components/route-download-modal/route-download-modal.component';
import { UploadPhotoComponent } from './components/upload-photo/upload-photo.component';
import { AppRouterModule } from './app.routing.module';
import { VerifyRouteComponent } from './pages/verify-route/verify-route.component';
import { SubNavItemComponent } from './components/navigation-view/sub-nav-item/sub-nav-item.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HistoryComponent,
    TentComponent,
    PublicRoutesComponent,
    RouteDetailComponent,
    MemberRoutesComponent,
    UploadRoutesComponent,
    LoginComponent,
    BecomeMemberComponent,
    SignUpComponent,
    AccountComponent,
    GuestBookViewComponent,
    ContactComponent,
    GalleryViewComponent,
    AlbumDetailComponent,
    AddAlbumComponent,
    AddPicturesComponent,
    VerifyAlbumsComponent,
    EventViewComponent,
    EventDetailComponent,
    NavigationViewComponent,
    RouteDownloadModalComponent,
    UploadPhotoComponent,
    VerifyRouteComponent,
    SubNavItemComponent,
  ],
  imports: [
    CommonModule,
    AppRouterModule,
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDialogModule,
    MatToolbarModule,
    MatMenuModule,
    FlexLayoutModule,
    GalleryModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatIconModule,
  ],
  providers: [GlobalEventsManager, MatDatepickerModule],
  entryComponents: [
    RouteDownloadModalComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
