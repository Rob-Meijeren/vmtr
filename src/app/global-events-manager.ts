import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class GlobalEventsManager {

  private showLoggedInNavBar$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
  public showNavBarEmitter: Observable<boolean> = this.showLoggedInNavBar$.asObservable();

  constructor() { }

  showLoggedInNavBar(ifShow: boolean) {
    this.showLoggedInNavBar$.next(ifShow);
  }

}
