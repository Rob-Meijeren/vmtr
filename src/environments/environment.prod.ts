export const environment = {
  production: true,
  api_base: 'https://api.vmtr.nl',
  api_url: 'https://api.vmtr.nl/api'
};
